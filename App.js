import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import Router from './src/Router';

class App extends Component {

  componentDidMount() {
    const config = {
        apiKey: 'AIzaSyA6HSp1fPNim0b8Jptma3RLHCVtHW5_ei8',
        authDomain: 'spacebox-7bf09.firebaseapp.com',
        databaseURL: 'https://spacebox-7bf09.firebaseio.com',
        projectId: 'spacebox-7bf09',
        storageBucket: 'spacebox-7bf09.appspot.com',
        messagingSenderId: '120891801400'
      };
      firebase.initializeApp(config);
  }

  render() {
    return (
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <Router />
      </Provider>
    );
  }
}

export default App;
