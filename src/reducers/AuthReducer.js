import { 
  REGISTER_FORM_UPDATE, 
  REGISTER_USER_FAIL, 
  REGISTER_USER_SUCCESS,
  LOGIN_FORM_UPDATE,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER
} from '../actions/types';

const INITIAL_STATE = { 
  loginPhone: '', 
  loginPassword: '', 
  registerEmail: '', 
  registerPassword: '', 
  registerName: '',
  user: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REGISTER_FORM_UPDATE:      
      return { ...state, [action.payload.prop]: action.payload.value };
    case REGISTER_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, user: action.payload };
    case REGISTER_USER_FAIL:
      return { ...state, ...INITIAL_STATE };
    case LOGIN_FORM_UPDATE:      
      return { ...state, [action.payload.prop]: action.payload.value };
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, user: action.payload };
    case LOGIN_USER_FAIL:
      return { ...state, ...INITIAL_STATE };
    case LOGOUT_USER:
      return { ...state, ...INITIAL_STATE };
    default: 
      return state;
  }
};
