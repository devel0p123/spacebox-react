import moment from 'moment';
import { 
  CATEGORY_SELECTION_CHANGE,
  PICKER_TOGGLE,
  UPDATE_DATE,
  UPDATE_TIME,
  LOGOUT_USER
} from '../actions/types';

const INITIAL_STATE = { 
  categories: ['Chair', 'Table', 'Bed', 'Cupboard'],
  selected: [],
  pickerVisible: false,
  pickerMode: 'date',
  date: 'Select Date',
  time: 'Select Time'
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CATEGORY_SELECTION_CHANGE:
      return { ...state, selected: action.payload };
    case PICKER_TOGGLE:
      return { ...state, pickerVisible: action.payload.show, pickerMode: action.payload.mode };
    case UPDATE_DATE:
      return { 
        ...state, 
        date: moment(action.payload).format('MMM DD YYYY'), 
        pickerVisible: false 
      };
    case UPDATE_TIME:
      return { ...state, time: moment(action.payload).format('h:mm a'), pickerVisible: false };
    case LOGOUT_USER:
      return { ...state, ...INITIAL_STATE };
    default: 
      return state;
  }
};
