import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ImageBackground, Image, FlatList, Text } from 'react-native';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


class OrderDetails extends Component {

  onButtonPress() {
    const { phone, password } = this.props;
    this.props.loginUser(phone, password);
  }

  _keyExtractor = (item, index) => item.id;

  renderItem({ item }) {
    return (
      <View style={styles.rowStyle}>
        <Image source={require('../images/selected.png')} style={styles.checkStyle}/>
        <Text style={styles.labelStyle}>{item.label}</Text>
      </View>
    );    
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground source={require('../images/background.png')} style={styles.headerImage}>
          <Image source={require('../images/order-placed.png')} style={styles.headerIcon} />
        </ImageBackground>
        <FlatList
          keyExtractor={this._keyExtractor}
          data={this.props.selected}
          renderItem={this.renderItem}
        />
      </View>
    );
  }
}

const styles = {
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
    height: responsiveHeight(100),
    width: responsiveWidth(100)
  },
  headerImage: {
    height: responsiveHeight(40),
    alignItems: 'center',
    position: 'relative',
    justifyContent: 'center'
  },
  headerIcon: {
    height: responsiveHeight(15),
    resizeMode: 'contain'
  },
  rowStyle: {
    flexDirection: 'row',
    width: responsiveWidth(100),
    alignItems: 'center',
    borderBottomColor: 'rgba(29,29,38,0.05)',
    borderBottomWidth: 1.5,
    paddingLeft: responsiveWidth(5),
    height: responsiveHeight(10)
  },
  checkStyle: {
    width: responsiveWidth(2.5),
    resizeMode: 'contain',
    marginLeft: responsiveWidth(2.5)
  },
  labelStyle: {
    fontFamily: 'Avenir',
    fontSize: responsiveFontSize(2),
    letterSpacing: 1,
    marginLeft: responsiveWidth(5),
    color: '#1d1d26'
  },

};

const mapStateToProps = state => {
  return {
    selected: state.category.selected
  };
};

export default connect(mapStateToProps, {})(OrderDetails);
