import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ImageBackground, Image } from 'react-native';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { loginFormUpdate, loginUser } from '../actions';
import { Card, CardSection, Input, Button } from './common';


class LoginForm extends Component {

  onButtonPress() {
    const { phone, password } = this.props;
    this.props.loginUser(phone, password);
  }

 
  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground source={require('../images/background.png')} style={styles.headerImage}>
          <Image source={require('../images/signin.png')} style={styles.headerIcon} />
        </ImageBackground>
        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <Input
              placeholder="PHONE"
              onChangeText={
                value => this.props.loginFormUpdate({
                  prop: 'loginPhone', value 
                })
              }
              value={this.props.phone}
            />
            <Image source={require('../images/phone.png')} style={styles.icon} />
          </View>
          <View style={styles.divider} />
          <View style={styles.inputContainer}>
            <Input 
              secureTextEntry
              placeholder="PASSWORD"
              onChangeText={
                value => this.props.loginFormUpdate({
                  prop: 'loginPassword', value 
                })
              }
              value={this.props.password}
            />
            <Image source={require('../images/password.png')} style={styles.icon} />
          </View>
          <View style={styles.divider} />
        </View>
        <Button style={styles.loginButton} onPress={this.onButtonPress.bind(this)}>
          SIGN IN
        </Button>
      </View>
    );
  }
}

const styles = {
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
    height: responsiveHeight(100),
    width: responsiveWidth(100)
  },
  headerImage: {
    height: responsiveHeight(30),
    alignItems: 'center',
    position: 'relative',
    justifyContent: 'center'
  },
  formContainer: {
    padding: responsiveWidth(10),
    paddingTop: responsiveHeight(5)
  },
  headerIcon: {
    height: responsiveHeight(7.5),
    resizeMode: 'contain'
  },
  inputContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: responsiveHeight(10)
  },
  icon: {
    width: responsiveWidth(5),
    aspectRatio: 1,
    resizeMode: 'contain'
  },
  divider: {
    width: responsiveWidth(80),
    height: 1.5,
    backgroundColor: 'rgba(29,29,38,0.05)'
  },
  loginButton: {
    height: responsiveHeight(9),
    width: responsiveWidth(100),
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#00b9ff',
  }

};

const mapStateToProps = state => {
  return {
    phone: state.auth.loginPhone,
    password: state.auth.loginPassword,
  };
};

export default connect(mapStateToProps, { loginFormUpdate, loginUser })(LoginForm);
