import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, View, Image, AsyncStorage } from 'react-native';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { Actions } from 'react-native-router-flux';
import SplashScreen from 'react-native-splash-screen'
import { openSignIn, openSignUp } from '../actions';
import { Button } from './common';


class Welcome extends Component {
  onSignInPress() {
    Actions.login();
  }

  onSignUpPress() {
    Actions.signUp();
  }

  componentWillMount() {
    AsyncStorage.getItem('AUTH_TOKEN', (err, token) => {
      if (token) {
        Actions.home();
      }
    });
  }

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <ImageBackground source={require('../images/background.png')} style={styles.backgroundImage}>
        <Image source={require('../images/logo.png')} style={styles.logo} />
        <View style={styles.buttonContainer}>
          <Button style={styles.signInButton} onPress={this.onSignInPress.bind(this)}> 
            SIGN IN 
          </Button>
          <Button style={styles.signUpButton} onPress={this.onSignUpPress.bind(this)}> 
            SIGN UP 
          </Button>
        </View>
      </ImageBackground>
    );
  }
}

const styles = {
  backgroundImage: {
    flex: 1,
    flexDirection: 'column',
    width: null,
    height: null,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  logo: {
    width: responsiveWidth(100),
    resizeMode: 'contain',
    marginTop: responsiveHeight(40)
  },
  buttonContainer: {
    height: responsiveHeight(9),
    width: responsiveWidth(100),
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0
  },
  signUpButton: {
    width: responsiveWidth(50),
    backgroundColor: '#d667cd'
  },
  signInButton: {
    width: responsiveWidth(50),
    backgroundColor: '#00b9ff',
  }

};

export default connect(null, { openSignIn, openSignUp })(Welcome);
