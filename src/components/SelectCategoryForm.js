import React, { Component } from 'react';
import { Image, View, Text, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import Animatable, { createAnimatableComponent } from 'react-native-animatable';
import DateTimePicker from 'react-native-modal-datetime-picker';
import SelectMultiple from 'react-native-select-multiple';
import SplashScreen from 'react-native-splash-screen'
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { 
  categoryUpdate, 
  togglePicker,
  updateDate,
  updateTime,
  categorySubmit
} from '../actions';
import { Button } from './common';
const AnimatableView = createAnimatableComponent(View);

class SelectCategoryForm extends Component {
  
  componentDidMount() {
    SplashScreen.hide();
  }

  onSelectionsChange(selections) {
    this.props.categoryUpdate(selections);
  }

  onSubmit() {
    this.props.categorySubmit(this.props.selected, this.props.date, this.props.time);
  }

  showPicker(mode) {
    this.props.togglePicker(true, mode);
  }

  hidePicker() {
    this.props.togglePicker(false);
  }

  updateDateTime(dateTime) {
    if (this.props.pickerMode === 'date') {
      this.props.updateDate(dateTime);
    } else {
      this.props.updateTime(dateTime);
    }
  }

  renderLabel(label, style) {
    switch (label) {
      case 'Chair': 
        return (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.rowContentStyle}>
              <Text style={style}>{label}</Text>
              <Image style={styles.rowIcon} source={require('../images/chair.png')} />
            </View>
          </View>
        );
      case 'Table': 
        return (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.rowContentStyle}>
              <Text style={style}>{label}</Text>
              <Image style={styles.rowIcon} source={require('../images/table.png')} />
            </View>
          </View>
        );
      case 'Bed': 
        return (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.rowContentStyle}>
              <Text style={style}>{label}</Text>
              <Image style={styles.rowIcon} source={require('../images/bed.png')} />
            </View>
          </View>
        );
      default: 
        return (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={styles.rowContentStyle}>
              <Text style={style}>{label}</Text>
              <Image style={styles.rowIcon} source={require('../images/cupboard.png')} />
            </View>
          </View>
        );
    }
  }


  render() {
    return (
      <View style={styles.mainContainer}>
        <AnimatableView animation="fadeInDown">
          <SelectMultiple
            items={this.props.categories}
            selectedItems={this.props.selected}
            onSelectionsChange={this.onSelectionsChange.bind(this)}
            checkboxSource={require('../images/unselected.png')}
            selectedCheckboxSource={require('../images/selected.png')}
            checkboxStyle={styles.checkStyle}
            selectedCheckboxStyle={styles.checkStyle}
            labelStyle={styles.labelStyle}
            rowStyle={styles.rowStyle}
            renderLabel={this.renderLabel}
          />
          <View style={styles.dateTimePickerContainer}>
            <TouchableWithoutFeedback onPress={() => { this.showPicker('date'); }}>
              <View style={styles.dateTimePicker}>
                <Image source={require('../images/date.png')} style={styles.pickerIcon} />
                <Text style={styles.pickerText}>{this.props.date}</Text>
              </View>
            </TouchableWithoutFeedback>
            <View style={styles.pickerDivider} />
            <TouchableWithoutFeedback onPress={() => { this.showPicker('time'); }}>
              <View style={styles.dateTimePicker}>
                <Image source={require('../images/time.png')} style={styles.pickerIcon} />
                <Text style={styles.pickerText}>{this.props.time}</Text>
              </View>
            </TouchableWithoutFeedback>
            </View>
        </AnimatableView>
        <Button style={styles.saveButton} onPress={this.onSubmit.bind(this)}>
          DONE
        </Button>
        <DateTimePicker
          isVisible={this.props.pickerVisible}
          onConfirm={this.updateDateTime.bind(this)}
          onCancel={this.hidePicker.bind(this)}
          mode={this.props.pickerMode}
        />
      </View>
    );
  }
}

const styles = {
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
    height: responsiveHeight(100),
    width: responsiveWidth(100),
    position: 'relative'
  },
  checkStyle: {
    width: responsiveWidth(2.5),
    resizeMode: 'contain',
    marginLeft: responsiveWidth(2.5)
  },
  labelStyle: {
    fontFamily: 'Avenir',
    fontSize: responsiveFontSize(2),
    letterSpacing: 1,
    marginLeft: responsiveWidth(5),
    color: '#1d1d26'
  },
  rowStyle: {
   borderBottomColor: 'rgba(29,29,38,0.05)',
   borderBottomWidth: 1.5,
   height: responsiveHeight(10),
  },
  pickerDivider: {
    height: responsiveHeight(5),
    width: 1.5,
    backgroundColor: 'rgba(29,29,38,0.05)'
  },
  dateTimePickerContainer: {
    width: responsiveWidth(100),
    height: responsiveHeight(10),
    flexDirection: 'row',
    borderBottomColor: 'rgba(29,29,38,0.05)',
    borderBottomWidth: 1.5,
    alignItems: 'center',
  },
  dateTimePicker: {
    width: responsiveWidth(49),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  pickerIcon: {
    width: responsiveWidth(5),
    resizeMode: 'contain'
  },
  pickerText: {
    fontFamily: 'Avenir',
    fontSize: responsiveFontSize(1.75),
    letterSpacing: 1,
    color: '#1d1d26'
  },
  saveButton: {
    height: responsiveHeight(9),
    width: responsiveWidth(100),
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#d667cd',
  },
  rowContentStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: responsiveWidth(85),
    alignItems: 'center'
  },
  rowIcon: {
    width: responsiveWidth(6.5),
    resizeMode: 'contain'
  }
};

const mapStateToProps = state => {
  return {
    categories: state.category.categories,
    selected: state.category.selected,
    pickerVisible: state.category.pickerVisible,
    pickerMode: state.category.pickerMode,
    date: state.category.date,
    time: state.category.time
  };
};

export default connect(mapStateToProps, { 
  categoryUpdate,
  togglePicker,
  updateDate,
  updateTime,
  categorySubmit
})(SelectCategoryForm);
