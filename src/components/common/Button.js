import React from 'react';
import { Text, TouchableHighlight } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { createAnimatableComponent } from 'react-native-animatable';

const AnimatableButton = createAnimatableComponent(TouchableHighlight);

const Button = ({ onPress, children, style, animation }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <AnimatableButton onPress={onPress} style={[buttonStyle, style]} underlayColor={style.backgroundColor} activeOpacity={1} animation={animation}>
      <Text style={textStyle}>
        {children}
      </Text>
    </AnimatableButton>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontWeight: '600',
    paddingTop: 20,
    paddingBottom: 10,
    color: '#fff',
    fontFamily: 'Avenir',
    fontSize: responsiveFontSize(1.75),
    letterSpacing: 1
  },
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
  }
};

export { Button };
