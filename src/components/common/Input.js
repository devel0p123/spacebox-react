import React from 'react';
import { TextInput, View, Text } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';


const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
  const { inputStyle, labelStyle, containerStyle } = styles;

  return (
    <View style={containerStyle}>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        placeholderTextColor='#1d1d26'
        autoCorrect={false}
        style={inputStyle}
        value={value}
        underlineColorAndroid='rgba(0,0,0,0)'
        onChangeText={onChangeText}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#1d1d26',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: responsiveFontSize(1.5),
    letterSpacing: 1,
    fontFamily: 'Avenir',
    flex: 2,
    borderBottomWidth: 0
  },
  containerStyle: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
};

export { Input };
