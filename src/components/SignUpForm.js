import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ImageBackground, Image } from 'react-native';
import { responsiveHeight, responsiveWidth } from 'react-native-responsive-dimensions';
import { registerFormUpdate, registerUser } from '../actions';
import { Input, Button } from './common';


class SignUpForm extends Component {

  onButtonPress() {
    const { phone, password, name } = this.props;
    this.props.registerUser(phone, password, name);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground source={require('../images/background.png')} style={styles.headerImage}>
          <Image source={require('../images/signup.png')} style={styles.headerIcon}/>
        </ImageBackground>
        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <Input
              placeholder="PHONE"
              onChangeText={
                value => this.props.registerFormUpdate({
                  prop: 'registerPhone', value 
                })
              }
              value={this.props.registerPhone}
            />
            <Image source={require('../images/phone.png')} style={styles.icon} />
          </View>
          <View style={styles.divider} />
          <View style={styles.inputContainer}>
            <Input
              placeholder="NAME"
              onChangeText={
                value => this.props.registerFormUpdate({
                  prop: 'registerName', value 
                })
              }
              value={this.props.registerName}
            />
            <Image source={require('../images/name.png')} style={styles.icon} />
          </View>
          <View style={styles.divider} />
          <View style={styles.inputContainer}>
            <Input 
              secureTextEntry
              placeholder="PASSWORD"
              onChangeText={
                value => this.props.registerFormUpdate({
                  prop: 'registerPassword', value 
                })
              }
              value={this.props.registerPassword}
            />
            <Image source={require('../images/password.png')} style={styles.icon} />
          </View>
          <View style={styles.divider} />
        </View>
        <Button style={styles.registerButton} onPress={this.onButtonPress.bind(this)}>
          CREATE
        </Button>
      </View>
    );
  }
}

const styles = {
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
    height: responsiveHeight(100),
    width: responsiveWidth(100)
  },
  headerImage: {
    height: responsiveHeight(30),
    alignItems: 'center',
    position: 'relative',
    justifyContent: 'center',
  },
  headerIcon: {
    height: responsiveHeight(10),
    resizeMode: 'contain'
  },
  formContainer: {
    padding: responsiveWidth(10),
    paddingTop: responsiveHeight(5)
  },
  inputContainer: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: responsiveHeight(10)
  },
  icon: {
    width: responsiveWidth(5),
    aspectRatio: 1,
    resizeMode: 'contain'
  },
  divider: {
    width: responsiveWidth(80),
    height: 1.5,
    backgroundColor: 'rgba(29,29,38,0.05)'
  },
  registerButton: {
    height: responsiveHeight(9),
    width: responsiveWidth(100),
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#d667cd',
  }

};

const mapStateToProps = state => {
  return {
    phone: state.auth.registerPhone,
    password: state.auth.registerPassword,
    name: state.auth.registerName
  };
};

export default connect(mapStateToProps, { registerFormUpdate, registerUser })(SignUpForm);
