import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage, Platform, AlertIOS, ToastAndroid } from 'react-native';
import config from '../config';
import { 
  REGISTER_FORM_UPDATE, 
  REGISTER_USER_SUCCESS, 
  REGISTER_USER_FAIL,
  LOGIN_FORM_UPDATE,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER
} from './types';

const BASE_URL = config.base_url;

const isPhone = (phone) => {
  const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
  return re.test(phone);
};

axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    if (error.response.status === 401) {
      Actions.welcome();
      AsyncStorage.removeItem('AUTH_TOKEN');
    }
});

export const registerFormUpdate = ({ prop, value }) => {
  return {
    type: REGISTER_FORM_UPDATE,
    payload: { prop, value }
  };
};

export const loginFormUpdate = ({ prop, value }) => {
  return {
    type: LOGIN_FORM_UPDATE,
    payload: { prop, value }
  };
};

export const loginUser = (phone, password) => {
  return (dispatch) => {
    axios({
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      url: `${BASE_URL}user/auth`,
      data: { phone, password }
      })
      .then(response => {
        AsyncStorage.setItem('AUTH_TOKEN', response.data.result.token);
        if (response.data.success) {
          dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: response.data.result
          });
          Actions.home();
        }
      })
      .catch((Error) => {
        alert(Error);
        if (Platform.OS === 'ios') {
          AlertIOS.alert('Error', 'Invalid Phone or Password');
        } else {
          ToastAndroid.showWithGravity(
            'Invalid Phone or Password', 
            ToastAndroid.LONG, 
            ToastAndroid.BOTTOM, 
            25, 100
          );
        }
        dispatch({
          type: LOGIN_USER_FAIL
        });
      });
  };
};

export const registerUser = (registerPhone, registerPassword, registerName) => {
  return (dispatch) => {
    if (registerPhone === '' || registerName === '' || registerPassword === '') {
      if (Platform.OS === 'ios') {
        AlertIOS.alert('Error', 'All Fields Are Mandatory');
      } else {
        ToastAndroid.showWithGravity(
          'All Fields Are Mandatory', 
          ToastAndroid.LONG, 
          ToastAndroid.BOTTOM, 
          25, 100
        );
      }
    } else if (!isPhone(registerPhone)) {
      if (Platform.OS === 'ios') {
        AlertIOS.alert('Error', 'Invalid Phone');
      } else {
        ToastAndroid.showWithGravity(
          'Invalid Phone', 
          ToastAndroid.LONG, 
          ToastAndroid.BOTTOM, 
          25, 100
        );
      }
    } else {
      axios({
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        url: `${BASE_URL}user/`,
        data: { phone: registerPhone, name: registerName, password: registerPassword }
      })
      .then(response => {
        if (response.data.success) {
          console.l
          AsyncStorage.setItem(
            'AUTH_TOKEN', 
            response.data.user.authToken[response.data.user.authToken.length - 1]
          );
          Actions.home();
          dispatch({
            type: REGISTER_USER_SUCCESS,
            payload: response.data.user
          });
        } else {
          if (Platform.OS === 'ios') {
            AlertIOS.alert('Error', 'User Already Exists');
          } else {
            ToastAndroid.showWithGravity(
              'User Already Exists', 
              ToastAndroid.LONG, 
              ToastAndroid.BOTTOM, 
              25, 100
            );
          }
          dispatch({
            type: REGISTER_USER_FAIL
          });
        }
      });
    }
  };
};

export const logout = () => async (dispatch) => {
  const authToken = await AsyncStorage.getItem('AUTH_TOKEN');
  AsyncStorage.removeItem('AUTH_TOKEN');
  Actions.welcome();
  axios({
    method: 'put',
    headers: {
      'Content-Type': 'application/json',
      authorization: authToken
    },
    url: `${BASE_URL}user/logout/`,
    data: { authToken }
  })
  .then(() => {
    dispatch({
      type: LOGOUT_USER
    });
  });
};
