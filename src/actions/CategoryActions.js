import axios from 'axios';
import { AsyncStorage, Platform, AlertIOS, ToastAndroid } from 'react-native';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import { notification } from '../Push';
import config from '../config';
import { 
  CATEGORY_SELECTION_CHANGE,
  PICKER_TOGGLE,
  UPDATE_DATE,
  UPDATE_TIME,
  CATEGORY_FORM_SUBMIT,
  CATEGORY_FORM_SUBMIT_FAIL
} from './types';

const BASE_URL = config.base_url;

axios.interceptors.response.use((response) => {
    return response;
}, (error) => {
    if (error.response.status === 401) {
      Actions.welcome();
      AsyncStorage.removeItem('AUTH_TOKEN');
    }
});

export const categoryUpdate = (selections) => {
  return (dispatch) => {
    dispatch({
      type: CATEGORY_SELECTION_CHANGE,
      payload: selections
    });
  };
};

export const togglePicker = (show, mode) => {
  return {
    type: PICKER_TOGGLE,
    payload: { show, mode }
  };
};

export const updateDate = (date) => {
  return {
    type: UPDATE_DATE,
    payload: date
  };
};

export const updateTime = (time) => {
  return {
    type: UPDATE_TIME,
    payload: time
  };
};

export const categorySubmit = (categories, date, time) => {

  const dateTime = moment(date + ' '+ time).toDate();
  notification(dateTime);
  categories = categories.map(a => a.label);
  if(categories.length === 0 || date === 'Select Date' || time === 'Select Time') {
    if (Platform.OS === 'ios') {
      AlertIOS.alert('Error', 'Please fill all details');
    } else {
      ToastAndroid.showWithGravity(
        'Please fill all details', 
        ToastAndroid.LONG, 
        ToastAndroid.BOTTOM, 
        25, 100
      );
    }
    return (dispatch) => {
      dispatch({
        type: CATEGORY_FORM_SUBMIT_FAIL
      });
    };
  } else {
    Actions.order();
    return (dispatch) => {
      dispatch({
        type: CATEGORY_FORM_SUBMIT
      });
    };
  }
  
};
