export const LOGIN_FORM_UPDATE = 'login_form_update';
export const LOGIN_USER_SUCCESS = 'login_user_success'; 
export const LOGIN_USER_FAIL = 'login_user_fail';
export const REGISTER_FORM_UPDATE = 'register_form_update';
export const REGISTER_USER_SUCCESS = 'register_user_success'; 
export const REGISTER_USER_FAIL = 'register_user_fail';
export const LOGOUT_USER = 'logout_user';

export const CATEGORY_SELECTION_CHANGE = 'category_selection_change';
export const PICKER_TOGGLE = 'picker_toggle';
export const UPDATE_DATE = 'update_date';
export const UPDATE_TIME = 'update_time';
export const CATEGORY_FORM_SUBMIT = 'category_form_submit';
export const CATEGORY_FORM_SUBMIT_FAIL = 'category_form_submit_fail';
