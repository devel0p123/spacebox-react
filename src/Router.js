import React, { Component } from 'react';
import { View, Image, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { Scene, Router, Stack, ActionConst } from 'react-native-router-flux';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import LoginForm from './components/LoginForm';
import SignUpForm from './components/SignUpForm';
import SelectCategoryForm from './components/SelectCategoryForm';
import OrderDetails from './components/OrderDetails';
import Welcome from './components/Welcome';
import { logout } from './actions';


class RouterComponent extends Component {

  logoutPress() {
    this.props.logout();
  }

  render() {
    return (
      <Router backButtonImage={require('./images/back.png')} navigationBarStyle={styles.navBar} titleStyle={styles.navBarTitle} barButtonTextStyle={styles.barButtonTextStyle} barButtonIconStyle={styles.barButtonIconStyle}>
        <Stack key="root">
          <Scene key="welcome" component={Welcome} hideNavBar type={ActionConst.RESET}  />
          <Scene key="login" component={LoginForm} title="SIGN IN" />
          <Scene key="signUp" component={SignUpForm} title="SIGN UP" />
          <Scene 
            key="home" 
            component={SelectCategoryForm} 
            renderLeftButton={<View />} 
            title="SELECT TYPE AND TIME" 
            type={ActionConst.RESET} 
            renderRightButton={
              <TouchableWithoutFeedback onPress={() => { this.logoutPress(); }}>
                <Image style={styles.logout} source={require('./images/logout.png')} />
              </TouchableWithoutFeedback>
            }  
          />
          <Scene 
            key="order" 
            component={OrderDetails}
            title="YOUR ORDER" 
            renderRightButton={
              <TouchableWithoutFeedback onPress={() => { this.logoutPress(); }}>
                <Image style={styles.logout} source={require('./images/logout.png')} />
              </TouchableWithoutFeedback>
            }
          />
        </Stack>
      </Router>
    );
  }
}


const styles = {
  navBar: {
    backgroundColor: '#00b9ff',
    borderBottomWidth: 0,
  },
  navBarTitle: {
    color: '#FFFFFF',
    fontFamily: 'Avenir',
    fontSize: responsiveFontSize(1.75),
    letterSpacing: 1

  },
  barButtonTextStyle: {
    color: '#FFFFFF'
  },
  barButtonIconStyle: {
    tintColor: 'rgb(255,255,255)',
    color: '#FFFFFF',
    resizeMode: 'contain'
  },
  logout: {
    width: responsiveWidth(5),
    resizeMode: 'contain',
    marginRight: responsiveWidth(5)
  }
};

export default connect(null, { logout })(RouterComponent);
